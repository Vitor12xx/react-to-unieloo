import React, { useEffect, useState } from "react";
import Professional from "./components/Professional";
import axios from "axios";
import "./App.css";

const App = () => {
  const [response, setResponse] = useState([]);

  useEffect(() => {
    async function getJSON() {
      let response = await axios.get(
        "https://unieloo-sandbox.herokuapp.com/teste"
      );
      setResponse(response.data.data);
    }

    getJSON();
  }, []);

  return (
    <div className="App">
      <Professional professionals={response} />
    </div>
  );
};

// class App extends Component {
//   state = {
//     response: []
//   };

//   componentDidMount() {
//     axios.get("https://unieloo-sandbox.herokuapp.com/teste").then(response => {
//       console.log(response.data.data);
//       this.setState({ response: response.data.data });
//     });
//   }

//   render() {
//     return (
//       <div className="App">
//         <Professional professionals={this.state.response} />
//       </div>
//     );
//   }
// }

export default App;
