import React from "react";
import ProfessionalCard from "./ProfessionalCard";
import styled from "styled-components";

const Professional = props => {
  const FlexWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  `;

  return (
    <FlexWrapper>
      {props.professionals.map(prof => (
        <ProfessionalCard key={prof.id} prof={prof} />
      ))}
    </FlexWrapper>
  );
};

export default Professional;
