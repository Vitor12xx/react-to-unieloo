import React from "react";

const Services = props => {
  return props.service.map(service => {
    return (
      <li key={service.id}>
        {service.name} <b> R$: {service.value}</b>
      </li>
    );
  });
};

export default Services;
