import React from "react";
import Services from "./Services";
import styled from "styled-components";

const ProfessionalCard = props => {
  const prof = props.prof;

  const Container = styled.div`
    color: #ddd;
    display: flex;
    padding: 20px;
    margin: 7px;
    background: rgb(120, 0, 30);
    border: 1px solid #ccc;
    width: 500px;
    box-shadow: 1px 1px violet;
  `;

  const ImgContainer = styled.div`
    display: flex;
    flex-direction: column;
  `;

  const ProfileImg = styled.img`
    border-radius: 4px 4px 4px 4px;
    width: 150px;
    height: 150px;
  `;

  const NameSection = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
  `;

  const InfoSection = styled.section`
    padding-left: 20px;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  `;

  const SpecializationSection = styled.section`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
  `;

  const IconLink = styled.img`
    padding-right: 5px;
    width: 30px;
    height: 30px;
  `;

  const ServicesSection = styled.section`
    width: 100%;
  `;

  return (
    <Container>
      <ImgContainer>
        <ProfileImg alt={prof.User.name} src={prof.User.profileImg} />
        <NameSection>
          <h3>{prof.User.name}</h3>
        </NameSection>
      </ImgContainer>

      <InfoSection>
        <SpecializationSection>
          <IconLink alt="iconLink" src={prof.Specialization.iconLink} />
          <h1>{prof.Specialization.name}</h1>
        </SpecializationSection>

        <ServicesSection>
          <Services service={prof.Services} />
        </ServicesSection>
      </InfoSection>
    </Container>
  );
};

export default ProfessionalCard;
