# React to Unieloo

Essa página foi feita como tarefa pedida para vaga de estágio da empresa Unieloo !

Esse foi minha primeira vez codando com `React`. Antes disso eu tinha contato vendo meus colegas fazendo o front dos nossos projetos, especialmente na Hut8. Falando sinceramente eu me diverti usando o React e infelizmente me faltou tempo para aprender mais e assimilar o conhecimento com o que eu queria fazer. Queria ter feito muito mais, sei que não foi feito nem metade do que foi pedido mas mesmo assim vou enviar meu pequeno projeto.

Obrigado pessoal da Unieloo por ter feito a tarefa, gostei demais! Agradeço pela oportunidade.

Vitor O. Torino